#!/usr/bin/env sh

sass ./scss/dgm_base.scss ./css/dgm_base.css --style expanded
sass ./scss/ckeditor5.scss ./css/ckeditor5.css --style expanded
sass ./scss/weather.scss ./css/weather.css --style expanded
sass ./scss/snow.scss ./css/snow.css --style expanded
sass ./scss/libraries/glide.scss ./css/glide.css --style expanded
