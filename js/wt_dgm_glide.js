(function ($, Drupal, drupalSettings, once) {
  'use strict';
  drupalSettings.wt = drupalSettings.wt || {};
  drupalSettings.wt.glide = drupalSettings.wt.glide || {};
  drupalSettings.wt.glide.glide = drupalSettings.wt.glide.glide || [];
  drupalSettings.wt.glide.options = drupalSettings.wt.glide.options || {};
  drupalSettings.wt.glide.options.type = drupalSettings.wt.glide.options.type || 'carousel';
  drupalSettings.wt.glide.options.focusAt = drupalSettings.wt.glide.options.focusAt ?? 'center';
  drupalSettings.wt.glide.options.autoplay = drupalSettings.wt.glide.options.autoplay ?? 6000;


  /**
   * Create carousels with glide JS
   */
  Drupal.behaviors.wtGlide = Drupal.behaviors.wtGlide || {
    attach: function (context, settings) {
      let $elements = $(once('wtGlide', '.glide', context));
      $elements.each( function() {
        drupalSettings.wt.glide.glide.push( new Glide(this, drupalSettings.wt.glide.options).mount() );
      });
    }
  }
} (jQuery, Drupal, drupalSettings, once));
